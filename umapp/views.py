from django.urls import reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth import authenticate,login,logout
from django.contrib.auth.models import User
from .forms import *
from django.shortcuts import render,redirect
from django.views.generic import *
from django.db.models import Q
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

# Create your views here.
class HomeView(TemplateView):
	template_name='home.html'

class SignupView(FormView):
	template_name= "signup.html"
	form_class= SignupForm
	success_url="/"

	def form_valid(self, form):
		uname=form.cleaned_data['username']
		email=form.cleaned_data['email']
		password=form.cleaned_data['password']
		print(uname,email,password)
		User.objects.create_user(uname,email,password)

		return super().form_valid(form)



class SigninView(FormView):
	template_name='signin.html'
	form_class=SigninForm
	success_url="/"


	def form_valid(self,form):
		uname=form.cleaned_data["username"]
		pword=form.cleaned_data["password"]
		print(uname,pword)
		user=authenticate(username=uname,password=pword)
		if user is not None:
			login(self.request,user)
		else:
			return render(self.request,"signin.html",
				{"error":"invalid username or password","form":form})

		return super().form_valid(form)

class SignoutView(View):
	def get(self,request):
		logout(request)
		return redirect("/signin/")
'''
class AdminHomeView(LoginRequiredMixin,TemplateView):
	template_name="adminhome.html"
	login_url="/signin/"
'''


class AdminHomeView(TemplateView):
	template_name="adminhome.html"
	def dispatch(self,request,*args,**kwargs):
		if request.user.is_authenticated:
			pass
		else:
			return redirect("/signin/")

		return super().dispatch(request,*args,**kwargs)


class AdminBlogCreateView(LoginRequiredMixin,CreateView):
	template_name="adminblogcreate.html"
	form_class=BlogForm
	success_url="/um-admin"
	def form_valid(self,form):
		logged_in_user=self.request.user
		form.instance.author=logged_in_user


		return super().form_valid(form)


class BlogListView(ListView):
	template_name = 'bloglist.html'
	queryset = Blog.objects.all()
	context_object_name = "allblogs"


class BlogDetailView(DetailView):
	template_name = "blogdetail.html"
	model = Blog
	context_object_name = "blogobject"

	def get_context_data(self,**kwargs):
		context=super().get_context_data(**kwargs)
		context["form"]=CommentForm
		return context


class BlogCommentView(CreateView):
	template_name="blogcomment.html"
	form_class=CommentForm
	success_url="/blog/list/"

	def dispatch(self,request,*args,**kwargs):
		if not request.user.is_authenticated:
			return redirect("/signin/")
		return super().dispatch(request,*args,**kwargs)



	def form_valid(self,form):
		form.instance.commenter=self.request.user
		blog_id=self.kwargs["pk"]
		blog=Blog.objects.get(id=blog_id)
		form.instance.blog=blog


		return super().form_valid(form)

	def get_success_url(self):
		blog_id=self.kwargs["pk"]
		url_to_redirect="/blog/"+str(blog_id)+ "/detail/"

		return url_to_redirect


class PasswordChangeView(FormView):
	template_name='passwordchange.html'
	form_class=PasswordChangeForm
	success_url=reverse_lazy('umapp:home')

	def form_valid(self, form):
		a = form.cleaned_data['old_password']
		b = form.cleaned_data['new_password']
		logged_in_user = self.request.user
		user = authenticate(username=logged_in_user.username, password=a)
		if user is not None:
			logged_in_user.set_password(b)
			logged_in_user.save()
		else:
			return render(self.request, "passwordchange.html", {
				'error': "Your old password is incorrect", 
				'form': form
				})

		return super().form_valid(form)


class SearchView(TemplateView):
	template_name= "search.html"

	def get_context_data(self,**kwargs):
		context= super().get_context_data(**kwargs)
		keyword= self.request.GET["barshat"]
		#keyword= self.request.GET.get("barshat")
		a= Blog.objects.filter(Q(title__icontains = keyword) | Q(context__icontains= keyword))
		page = self.request.GET.get('page', 1)

		paginator = Paginator(a, 3)
		try:
			results = paginator.page(page)
		except PageNotAnInteger:
			results = paginator.page(1)
		except EmptyPage:
			results = paginator.page(paginator.num_pages)

		context["results"]= results



		return context


class CommentListView(ListView):
	template_name= "commentlist.html"
	model= Comment
	context_object_name= "comments"
	paginate_by= 2


	