from django.urls import path
from .views import *
# from .views import HomeView, ContactView

app_name="umapp"
urlpatterns=[
    path('', HomeView.as_view(), name='home'),
    path('signup/', SignupView.as_view(),name="signup"),
    path('signin/', SigninView.as_view(),name="signin"),
    path('signout/', SignoutView.as_view(),name="signout"),
    path('um-admin',AdminHomeView.as_view(),name="admin"),
    path('um-admin/blog/create',AdminBlogCreateView.as_view(),name="adminblogcreate"),
    path('blog/list/', BlogListView.as_view(), name="bloglist"),
    path('blog/<int:pk>/detail/', BlogDetailView.as_view(), name="blogdetail"),
    path('blog/<int:pk>/comment/', BlogCommentView.as_view(), name="blogcomment"),
    path('passwordchange/',PasswordChangeView.as_view(),name='passwordchange'),
    path('search/',SearchView.as_view(),name='search'),
    path('commentlist/',CommentListView.as_view(),name='comment'),
]